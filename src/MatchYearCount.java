import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;

public class MatchYearCount {

    public static void main(String[] args) {
        String path = "src/MatchData/matches.csv";
        String line = "";
        Map<String,Integer> yearCount = new TreeMap<>();

        try {
            BufferedReader br = new BufferedReader(new FileReader(path));
            br.readLine();
            //System.out.println(br.readLine());

            while((line = br.readLine()) != null){
                String[] values = line.split(",");

               if((yearCount.containsKey(values[1])) == false){
                   yearCount.put(values[1],1);
               }else{
                   int count = yearCount.get(values[1])+1;

                   yearCount.put(values[1],count);
               }
            }
            System.out.println(yearCount);

        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
}
