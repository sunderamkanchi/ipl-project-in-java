import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

public class PlayerOfTheMatch {

    public static void main(String[] args) {
        String path = "src/MatchData/matches.csv";
        String line = "";

        Map<String, TreeMap<String,Integer>> playerOfMatch = new TreeMap<>();

        try {
            BufferedReader br = new BufferedReader(new FileReader(path));
            br.readLine();

            while((line = br.readLine()) != null){
                String[] values = line.split(",");
                String player =values[13];

                if(!(playerOfMatch.containsKey(values[1]))){
                    playerOfMatch.put(values[1],new TreeMap<>());
                }
                if(playerOfMatch.containsKey(values[1])){
                    if(!(playerOfMatch.get(values[1]).containsKey(player))){
                        playerOfMatch.get(values[1]).put(player,0);
                    }
                }
                if((playerOfMatch.get(values[1]).containsKey(player))){
                    int count = playerOfMatch.get(values[1]).get(player)+1;
                    playerOfMatch.get(values[1]).put(player,count);

                }


            }
            System.out.println(playerOfMatch);

        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
}
